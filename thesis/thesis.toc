\babel@toc {english}{}
\contentsline {chapter}{\nonumberline Glossary}{ix}{chapter*.2}% 
\contentsline {chapter}{\numberline {1}General Introduction}{1}{chapter.1}% 
\contentsline {chapter}{\numberline {2}Computational Methods}{13}{chapter.2}% 
\contentsline {section}{\numberline {2.1}Schr{\"o}dinger Equation}{15}{section.2.1}% 
\contentsline {section}{\numberline {2.2}Born-Oppenheimer Approximation}{16}{section.2.2}% 
\contentsline {section}{\numberline {2.3}Computation of Electronic Energy}{18}{section.2.3}% 
\contentsline {subsection}{\numberline {2.3.1}Wavefunction based Methods}{19}{subsection.2.3.1}% 
\contentsline {subsection}{\numberline {2.3.2}Density Functional Theory}{21}{subsection.2.3.2}% 
\contentsline {subsection}{\numberline {2.3.3}Density Functional based Tight-Binding Theory}{26}{subsection.2.3.3}% 
\contentsline {subsection}{\numberline {2.3.4}Force Field Methods}{33}{subsection.2.3.4}% 
\contentsline {section}{\numberline {2.4}Exploration of PES}{35}{section.2.4}% 
\contentsline {subsection}{\numberline {2.4.1}Monte Carlo Simulations}{36}{subsection.2.4.1}% 
\contentsline {subsection}{\numberline {2.4.2}Classical Molecular Dynamics}{39}{subsection.2.4.2}% 
\contentsline {subsection}{\numberline {2.4.3}Parallel-Tempering Molecular Dynamics}{44}{subsection.2.4.3}% 
\contentsline {subsection}{\numberline {2.4.4}Global Optimization}{46}{subsection.2.4.4}% 
\contentsline {chapter}{\numberline {3}Investigation of Structural and Energetic Properties}{49}{chapter.3}% 
\contentsline {section}{\numberline {3.1}Computational Details}{49}{section.3.1}% 
\contentsline {subsection}{\numberline {3.1.1}SCC-DFTB Potential}{49}{subsection.3.1.1}% 
\contentsline {subsection}{\numberline {3.1.2}SCC-DFTB Exploration of PES}{50}{subsection.3.1.2}% 
\contentsline {subsection}{\numberline {3.1.3}MP2 Geometry Optimizations, Relative and Binding Energies}{52}{subsection.3.1.3}% 
\contentsline {subsection}{\numberline {3.1.4}Structure Classification}{52}{subsection.3.1.4}% 
\contentsline {section}{\numberline {3.2}Structural and Energetic Properties of Ammonium/Ammonia including Water Clusters}{53}{section.3.2}% 
\contentsline {subsection}{\numberline {3.2.1}General Introduction}{53}{subsection.3.2.1}% 
\contentsline {subsection}{\numberline {3.2.2}Results and Discussion}{55}{subsection.3.2.2}% 
\contentsline {subsubsection}{\numberline {3.2.2.1}Dissociation Curves and SCC-DFTB Potential}{55}{subsubsection.3.2.2.1}% 
\contentsline {subsubsection}{\numberline {3.2.2.2}Small Species: (H$_2$O)$_{1-3}${NH$_4$}$^+$ and (H$_2$O)$_{1-3}${NH$_3$}}{58}{subsubsection.3.2.2.2}% 
\contentsline {subsubsection}{\numberline {3.2.2.3}Properties of (H$_2$O)$_{4-10}${NH$_4$}$^+$ Clusters}{61}{subsubsection.3.2.2.3}% 
\contentsline {subsubsection}{\numberline {3.2.2.4}Properties of (H$_2$O)$_{4-10}${NH$_3$} Clusters}{68}{subsubsection.3.2.2.4}% 
\contentsline {subsubsection}{\numberline {3.2.2.5}Properties of (H$_2$O)$_{20}${NH$_4$}$^+$ Cluster}{73}{subsubsection.3.2.2.5}% 
\contentsline {subsection}{\numberline {3.2.3}Conclusions for Ammonium/Ammonia Including Water Clusters}{75}{subsection.3.2.3}% 
\contentsline {section}{\numberline {3.3}Structural and Energetic Properties of Protonated Uracil Water Clusters}{76}{section.3.3}% 
\contentsline {subsection}{\numberline {3.3.1}General Introduction}{76}{subsection.3.3.1}% 
\contentsline {subsection}{\numberline {3.3.2}Results and Discussion}{78}{subsection.3.3.2}% 
\contentsline {subsubsection}{\numberline {3.3.2.1}Experimental Results}{79}{subsubsection.3.3.2.1}% 
\contentsline {subsubsection}{\numberline {3.3.2.2}Calculated Structures of Protonated Uracil Water Clusters}{84}{subsubsection.3.3.2.2}% 
\contentsline {subsection}{\numberline {3.3.3}Conclusions on (H$_2$O)$_{n}$UH$^+$ clusters}{94}{subsection.3.3.3}% 
\contentsline {chapter}{\numberline {4}Dynamical Simulation of Collision-Induced Dissociation}{99}{chapter.4}% 
\contentsline {section}{\numberline {4.1}Experimental Methods}{99}{section.4.1}% 
\contentsline {subsection}{\numberline {4.1.1}Principle of TCID}{101}{subsection.4.1.1}% 
\contentsline {subsection}{\numberline {4.1.2}Experimental Setup}{102}{subsection.4.1.2}% 
\contentsline {section}{\numberline {4.2}Computational Details}{104}{section.4.2}% 
\contentsline {subsection}{\numberline {4.2.1}SCC-DFTB Potential}{104}{subsection.4.2.1}% 
\contentsline {subsection}{\numberline {4.2.2}Collision Trajectories}{105}{subsection.4.2.2}% 
\contentsline {subsection}{\numberline {4.2.3}Trajectory Analysis}{106}{subsection.4.2.3}% 
\contentsline {section}{\numberline {4.3}Dynamical Simulation of Collision-Induced Dissociation of Protonated Uracil Water Clusters}{107}{section.4.3}% 
\contentsline {subsection}{\numberline {4.3.1}Introduction}{107}{subsection.4.3.1}% 
\contentsline {subsection}{\numberline {4.3.2}Results and Discussion}{108}{subsection.4.3.2}% 
\contentsline {subsubsection}{\numberline {4.3.2.1}Statistical Convergence}{108}{subsubsection.4.3.2.1}% 
\contentsline {subsection}{\numberline {4.3.3}Time-Dependent Proportion of Fragments}{111}{subsection.4.3.3}% 
\contentsline {subsection}{\numberline {4.3.4}Proportion of Neutral Uracil Loss and Total Fragmentation Cross Sections for Small Clusters}{114}{subsection.4.3.4}% 
\contentsline {subsection}{\numberline {4.3.5}Behaviour at Larger Sizes, the Cases of (H$_2$O)$_{11, 12}$UH$^+$}{124}{subsection.4.3.5}% 
\contentsline {subsection}{\numberline {4.3.6}Mass Spectra of Fragments with Excess Proton}{128}{subsection.4.3.6}% 
\contentsline {subsection}{\numberline {4.3.7}Conclusions about CID of (H$_2$O)$_{n}$UH$^+$}{131}{subsection.4.3.7}% 
\contentsline {section}{\numberline {4.4}Dynamical Simulation of Collision-Induced Dissociation for Pyrene Dimer Cation}{133}{section.4.4}% 
\contentsline {subsection}{\numberline {4.4.1}Introduction}{133}{subsection.4.4.1}% 
\contentsline {subsection}{\numberline {4.4.2}Calculation of Energies}{135}{subsection.4.4.2}% 
\contentsline {subsection}{\numberline {4.4.3}Simulation of the Experimental TOFMS}{137}{subsection.4.4.3}% 
\contentsline {subsection}{\numberline {4.4.4}Results and Discussion}{139}{subsection.4.4.4}% 
\contentsline {subsubsection}{\numberline {4.4.4.1}TOFMS Comparison}{139}{subsubsection.4.4.4.1}% 
\contentsline {subsubsection}{\numberline {4.4.4.2}Molecular Dynamics Analysis}{140}{subsubsection.4.4.4.2}% 
\contentsline {subsection}{\numberline {4.4.5}Conclusions about CID of Py$_2^+$}{156}{subsection.4.4.5}% 
\contentsline {chapter}{\numberline {5}General Conclusions and Perspectives}{159}{chapter.5}% 
\contentsline {section}{\numberline {5.1}General Conclusions}{159}{section.5.1}% 
\contentsline {section}{\numberline {5.2}Perspectives}{162}{section.5.2}% 
\contentsline {chapter}{References}{165}{chapter*.83}% 
