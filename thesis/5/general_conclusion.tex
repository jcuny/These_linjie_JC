% this file is called up by thesis.tex
% content in this file will be fed into the main document
%: ----------------------- name of chapter  -------------------------
\chapter{General Conclusions and Perspectives}

\section{General Conclusions}

As stated in the general introduction, the goal of this thesis was to go a step further into the theoretical description of properties
of molecular clusters in the view to complement complex experimental measurements. It has focused on two different types of
molecular clusters. I have first investigated water clusters containing an impurity, \textit{i.e.} an additional ion or molecule.
First, I have studied ammonium and ammonia water clusters in order to thoroughly explore their PES to characterize in details
low-energy isomers for various cluster sizes. I have then tackled the study of protonated uracil water clusters through two aspects:
characterize low-energy isomers and model collision-induced dissociation experiments to probe dissociation mechanism in relation
with recent experimental measurements by S. Zamith and J.-M. l'Hermite. Finally, I have addressed the study of the pyrene dimer cation
to explore collision trajectories, dissociation mechanism, energy partition, mass spectra, and cross-section. These four studies have
been organized in two chapters, each one gathering two studies involving similar computational tools. Below are gathered the main 
conclusions obtained along this thesis. 

\textbf{Structural and energetic properties.}
The structures and binding energies of the lowest-energy isomers of   (H$_2$O)$_{1-10}${NH$_4$}$^+$  and (H$_2$O)$_{1-10}$NH$_3$
clusters were obtained through a synergistic use of  SCC-DFTB and PTMD. The reported low energy isomers were further optimized at
the MP2/Def2TZVP level of theory. In order to improve the description of sp$^3$ nitrogen, I have proposed a modified set of  N-H
parameters. Through comparing the configurations and binding energies of the lowest-energy isomers obtained at SCC-DFTB an
 MP2/Def2TZVP levels and by comparing the corresponding results to the literature, I demonstrated that this modified set of NH
 parameters is accurate enough to model both ammonia and ammonium water clusters. This work has thus allowed to report a number
 of new low-energy isomers for the studied species. Finally, PTMD simulation of (H$_2$O)$_{20}${NH$_4$}$^+$ was conducted and the
 heat capacity curve of this aggregate was obtained. It is in agreement with previous results reported in the literature.
  
A similar exploration of the PES of (H$_2$O)$_{1-7, 11, 12}$UH$^+$ clusters was also performed. The reported low-energy isomers
for these systems are all new and therefore constitute new data set to discuss and analyse the hydration properties of RNA nucleobases.
They also complement available structures already reported for the non-protonated (H$_2$O)$_{n}$UH$^+$ species. These structures
have also helped use to provide preliminary explanations to recent collision-induced measurements performed by S. Zamith and J.-M. 
l'Hermite. In particular, I show that  when there are only 1 or 2 water molecules, the excess proton is chemically bond to the uracil.
When there are 3 or 4 water molecules, the proton is still bound to the uracil but it has a tendency to be transferred toward an adjacent water
molecule. From $n$ = 5 and above, clusters contain enough water molecules to allow for a net separation between uracil and the
excess proton. The latter is often bound to a water molecule which is separated from uracil by at least one other water molecule.
In the context of a direct dissociation mechanism, the nature of these isomers and the localisation of the proton as a function of
cluster size, helps in analysing the nature of the fragments and the location of the proton on them.

These two studies finally provide a new proof that SCC-DFTB, when combined to efficient enhanced sampling methods, is a powerful
tool to  explore complex potential energy surfaces of molecular aggregates.  They have already given rise to two publications \cite{Simon2019,Braud2019}
and one other publication is in preparation.

\textbf{Collision-induced dissociation.}
The SCC-DFTB simulations  conducted to model collision-induced dissociation of (H$_2$O)$_{1-7,11,12}$UH$^+$ clusters
and pyrene dimer cation were presented. These simulations have provided a wealth of important information to complement recent
experimental CID measurements.

For the collision simulations of (H$_2$O)$_{1-7,11,12}$UH$^+$ clusters at constant center of mass collision energy, the theoretical proportion
of formed neutral \textit{vs.} protonated uracil containing clusters, total fragmentation cross sections as well as the mass spectra of charged
fragments are consistent with the experimental data which highlights the accuracy of the simulations. They allow to probe which fragments
are formed on the short time scale and rationalize the location of the excess proton on these fragments. Analyses of the time evolution of
the fragments populations and theoretical and experimental branching ratios  indicate that (H$_2$O)$_{1-7}$UH$^+$ engage a direct/shattering
mechanism (dissociation on a very short time scale) after collision whereas for (H$_2$O)$_{11-12}$UH$^+$ a significant contribution of structural
rearrangements occur. This suggests that a contribution of a statistical mechanism is more likely to occur for larger species such as
(H$_2$O)$_{11-12}$UH$^+$. Such study is almost unique as the modelling of the dissociation of aqueous aggregates is very scarce in the literature.
This study thus demonstrates that explicit molecular dynamics simulations at the SCC-DFTB level appear as a key tool to complement collision-induced
dissociation experiments of hydrated molecular clusters. This study opens new possibility in the domain and I hope it will motivate new experimental
measurements. One publication devoted to this study is in preparation.

Dynamical simulations of collision between Py$_2^+$ and argon at different center of mass collision energies, between 2.5 and 30.0 eV, were conducted.
Collision process, dissociation path, energy partition and distribution, and the efficiency of energy transfer were deeply explored form these simulations that
have provided valuable reference for the CID study of larger PAH cation clusters. The simulated TOFMS of parent and dissociated products were obtained
from the combination of MD simulations and PST to address the short and long timescales dissociation, respectively. The agreement between the simulated
and measured mass spectra suggests that the main processes are captured by this approach.  It appears that the TOFMS spectra mostly result from dimers
dissociating on short timescales (during the MD simulation) and the remaining minor contribution results from dimers dissociating at longer timescales
(the second step, during PST calculation). This indicates that Py$_2^+$ primarily engages a direct dissociation path after collision. The dynamical
simulations show that the outcome of the trajectories either toward a dissociation or a redistribution
of the transferred energy strongly depends on the initial collision conditions. Intramolecular fragmentation of the monomers occurs only for 
collision energies above 25 eV. At low collision energies, the dissociation cross section increases with collision energies whereas it remains almost
constant for collision energies greater than 10-15~eV.  The analysis of the kinetic energy partition as a function of the collision energy shows the
absorbed energy is shared between the dissociative modes and the heating of individual monomers. It shows that above 7.5~eV, increasing the
collision energy mostly results in an increase of the intramolecular energy. Finally, the analysis of energy transfer efficiency within the dimer suggests
that direct dissociation is too fast to allow significant thermalization of the system. On the other hand, when there is no dissociation, thermalization
can occur with a faster equilibration between the intramolecular modes of the two units than with the intermolecular modes.   
This study has given rise to two publications.\cite{Zamith2020threshold,Zheng2021}

%\break

\section{Perspectives}

This thesis has addressed various problems, on different molecular clusters, and has involved a range of theoretical methodologies that are not 
common way in computational chemistry. Various and very exciting perspectives can be therefore be considered in future studies:

\begin{itemize}

\item[$\bullet$] 
The newly proposed set of N-H parameters could be used to explore the low-energy structures and properties of a much larger range of systems
of atmospheric interest. Indeed the structure of pure (NH$_3$)$_m$ clusters as well as (NH$_3$)$_m$H$^+$, (H$_2$O)$_n$(NH$_3$)$_m$, and
(H$_2$O)$_n$(NH$_3$)$_m$H$^+$ clusters have been hardly addressed in the literature mainly due to the lake of properly defined force field for
these systems. The transferability of SCC-DFTB would suggests that the potential I developed could also applied to these systems. This is an ongoing
work that I have recently  initiated. More interesting and also complicated is the study of water clusters containing a mix of nitrogen and sulphur
compounds, for instance, ammonium and sulfate ion. These species, their conjugated basis and acid in combination with dimethylamine and
water molecules represent the basis for nucleation of atmospheric particles. The chemical complexity induced by their mixing in force field
simulations one the one hand, and the system size needed for proper molecular simulations on the other hand, suggest that SCC-DFTB
has a major role to play in the theoretical description of these species.

\item[$\bullet$] It would also be of great interest to pursue dynamical simulations of protonated uracil water clusters. Indeed, the work I have
presented in this thesis still suffers from some lacks. First, it would be of high interest to look at the influence of collision energy, both lower or
higher, on the dissociation mechanism as a function of the cluster size. By implementing a similar methodology  as for the study of Py$_2^+$,
it would be possible to extract important new information about energy partition and dissociation mechanism. Those can be of interest to other
aqueous aggregates. In other important point is the inclusion of nuclear quantum effects in the simulations. Indeed, as the experiments are
performed at very low temperatures, the quantum nature of the proton can play an important role that has been neglected in the present thesis.

\item[$\bullet$] The dynamical simulations for collision-induced dissociation of pyrene dimer cation can be extended to PAHs water clusters
to complement recent experiments on these systems.

\item[$\bullet$] Finally, all the simulations of water clusters performed within this thesis were performed in the electronic ground state. To model
what can occur in the atmosphere or interstellar medium, it would be of interest to investigate solvation effects on organic/inorganic molecules
brought in an electronic excited state. To do so,  the TD-DFTB method need to be implemented and tested as such simulation would involve a number
of additional theoretical complexities. This would allow to calculate both absorption spectra from electronic ground state and emission spectra from
electronic excited state of organic/inorganic molecule containing water clusters.


\end{itemize}

% change according to folder and file names
\ifpdf
    \graphicspath{{X/figures/PNG/}{X/figures/PDF/}{X/figures/}}
\else
    \graphicspath{{X/figures/EPS/}{X/figures/}}
\fi

%: ----------------------- contents from here ------------------------





